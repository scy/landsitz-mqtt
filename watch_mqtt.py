from os import environ
from time import monotonic, time

import paho.mqtt.client as mqtt
import requests


GRAPHITE_INTERVAL = 5
GONE_DARK_AFTER = 30
BELL_TOPIC = "ramalam/sensor/last_bell_press/state"
DOOR_TOPIC = "amtill/main_door/last_open_ts"
FLAP_TOPIC = "amtill/letterbox/flap_move_ts"
UPTIME_PATTERNS = ["amtill/power/+/sensor/uptime/state"]
ENVIRON_PATTERNS = ["amtill/+/temperature_c",
                    "amtill/+/humidity_pct",
                    "amtill/+/pressure_hpa",
                    "amtill/+/iaq",
                    "amtill/+/iaq_accuracy",
                    "amtill/+/eco2_ppm",
                    "amtill/+/sensor_battery_pct",
                    "amtill/power/+/power_w",
                    "amtill/power/+/current_a",
                   ]


graphite_values = []
graphite_last_sent = monotonic()
uptimes = {}


def post(*args, **kwargs):
    try:
        return requests.post(*args, **kwargs)
    except:
        return None


def pushover(app_key, message, sound="pushover"):
    post(
        "https://api.pushover.net/1/messages.json",
        json={
            "user": environ["PUSHOVER_USER"],
            "token": app_key,
            "message": message,
            "sound": sound,
        },
    )


def graphite(values):
    global graphite_values, graphite_last_sent
    if len(graphite_values) > 1000:
        # Something seems very wrong.
        graphite_values = []
    graphite_values += values
    now = monotonic()
    if now > graphite_last_sent + GRAPHITE_INTERVAL:
        print(f"Sending {len(graphite_values)} value(s) to Graphite.")
        graphite_last_sent = now
        post(
            environ["GRAPHITE_URL"],
            headers={
                "Authorization": f"Bearer {environ['GRAPHITE_APIKEY']}",
            },
            json=graphite_values,
        )
        graphite_values = []


def handle_uptime(device, uptime):
    global uptimes
    now = monotonic()
    if device not in uptimes:
        uptimes[device] = (uptime, now)
        return
    prev_uptime = uptimes[device][0]
    if prev_uptime > uptime:
        # Uptime jumped backwards.
        uptimes[device] = (uptime, now)
        return pushover(
            environ["PUSHOVER_ALERT_APP"],
            f"Device '{device}' just rebooted after {prev_uptime} seconds of uptime.",
            "siren",
        )
    uptimes[device] = (uptime, now)
    timeout = now - GONE_DARK_AFTER
    for device, (uptime, seen) in uptimes.items():
        if seen is not None and seen < timeout:
            uptimes[device] = (uptime, None)  # only alert once
            return pushover(
                environ["PUSHOVER_ALERT_APP"],
                f"Device '{device}' has gone dark after {uptime} seconds of uptime.",
                "siren",
            )


def on_connect(client, userdata, flags, rc):
    print(f"Connected with result code {rc}.")
    for topic in [BELL_TOPIC, DOOR_TOPIC, FLAP_TOPIC] + UPTIME_PATTERNS + ENVIRON_PATTERNS:
        client.subscribe(topic)


def on_message(client, userdata, msg):
    topic = msg.topic
    payload = msg.payload.decode()
    split_topic = topic.split("/")
    now = round(time())
    if split_topic[0] == "amtill" and topic not in [DOOR_TOPIC, FLAP_TOPIC]:
        if topic.find("/uptime/") != -1:
            handle_uptime(split_topic[2], float(payload))
        if topic.find("/sensor/uptime/state") != -1:
            # Get rid of the naming cruft before sending it to Graphite.
            split_topic = topic.replace("/sensor/uptime/state", "/uptime_s").split("/")
        return graphite([{
            "name": ".".join(split_topic),
            "value": round(float(payload)) if split_topic[-1].endswith(("_ppm", "iaq", "iaq_accuracy")) else float(payload),
            "interval": 10 if split_topic[1] in ["card10", "power"] else 60,
            "time": now,
        }])
    print(f"<{topic}> {payload}")
    if msg.retain:
        print("  (retained, ignoring)")
        return
    if topic == BELL_TOPIC:
        return pushover(
            environ["PUSHOVER_BELL_APP"],
            f"Ding Dong! ({payload})",
        )
    if topic == DOOR_TOPIC:
        return pushover(
            environ["PUSHOVER_BELL_APP"],
            f"Tür geht auf @ {payload}",
        )
    if topic == FLAP_TOPIC:
        return pushover(
            environ["PUSHOVER_FLAP_APP"],
            f"Klappe wackelt @ {payload}",
            "bugle",
        )


if __name__ == "__main__":
    client = mqtt.Client()
    client.on_connect = on_connect
    client.on_message = on_message
    client.connect("mqtt")
    client.loop_forever()
